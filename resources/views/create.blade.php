
<html lang="en">
<head>
    <title>Laravel Multiple File Upload Example</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <h3 class="jumbotron">Laravel Multiple File Upload</h3>
    <form method="post" action="{{url('form')}}" enctype="multipart/form-data">
        {{csrf_field()}}

        <div class="input-group control-group increment" >
            <input type="file" name="filename[]" class="form-control">
            <div class="input-group-btn">
                <button class="bt       n btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
            </div>
        </div>
        <div class="clone hide">
            <div class="control-group input-group" style="margin-top:10px">
                <input type="file" name="filename[]" class="form-control">
                <div class="input-group-btn">
                    <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary" style="margin-top:10px">Submit</button>

    </form>
</div>

<script type="text/javascript">


    $(document).ready(function() {

        $(".btn-success").click(function(){
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){
            $(this).parents(".control-group").remove();
        });

    });

</script>
<img src="http://127.0.0.1:8000/images/a.png">
<video width="320" height="240" controls>
    <source src="ab.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

{{--
<video id="my-video" class="video-js" controls preload="auto"
       poster="" data-setup="{}" style="width:100%">
    <source src="http://localhost:8000/images/abc.MP4" type='video/mp4'>

    <p class="vjs-no-js">

        To view this video please enable JavaScript, and consider upgrading to a web browser that
        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
</video>--}}

<video id="myVideo" controls autoplay>
    <source id="mp4_src" src="http://localhost:8000/images/abc.MP4" type="video/mp4">

    Your browser does not support HTML5 video.
</video>

<script>
    function myFunction() {
        document.getElementById("mp4_src").src = "http://localhost:8000/images/abc.MP4";
        document.getElementById("myVideo").load();
    }
</script>
</video>
</body>
</html>