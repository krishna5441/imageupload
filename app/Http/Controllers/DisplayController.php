<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DisplayController extends Controller
{
    //
    public function home() {
        // Your logic here
        return view('display');
    }
}
